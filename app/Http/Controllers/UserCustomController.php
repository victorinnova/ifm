<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Hash;

class UserCustomController extends Controller
{

    public function index($id)
    {
        if(auth()->user()->rol == 1){
        $user = User::find($id);
        return view('admin.user_edit')->with('user',$user);
    }
    else{
        return view('welcome');
    }
    }
    public function showAll(){
        if(auth()->user()->rol == 1){
            $usuarios = User::all();
            $maquina = DB::table('maquinas')->where('user_id',auth()->user()->id)->first();
            return view('admin.users_list')->with('usuarios', $usuarios)->with('maquinas',$maquina);
        }
        else{
            return view('welcome');
        }
    }
    public function create()
    {
     return view('admin.user_create');
        
    }
    public function create_web(Request $request)
    {
        $usuarios = User::all();
        if(auth()->user()->rol == 1){
       if($request->email != ''){
            $email = $request->email;
        }else{
            $email="";
        }
        if($request->bloqueado != ''){
            $bloqueado = $request->bloqueado;
        }else{
            $bloqueado=false;

        }
        if($request->name != ''){
            $name = $request->name;
        }else{
            $name="0";
        }
        if($request->password != ''){
            $password=Hash::make($request->password);
        }else{
            $password = 0;
        }
        if($request->apellido != ''){
            $apellido = $request->apellido;
        }else{
            $apellido ="";
        }
        if($request->codigo != ''){
            $codigo = $request->codigo;
        }else{
            $codigo ="";
        }
        if($request->rol != ''){
            $rol = $request->rol;
        }else{
            $rol ="0";
        }
        
        $maquinas= DB::table('maquinas')->where('user_id',auth()->user()->id)->first();
        User::create(['name' => $name, 'apellido'=>$apellido,'email'=>$email, 'bloqueado'=>$bloqueado,  'password'=>$password,'codigo'=>$codigo,'rol'=>$rol]);
        return redirect('admin/users_list');
    }
    else{
        return view('welcome');
    }
        
    }

    public function store(Request $request)
    {
        //
    }

    public function show_web(){

        return view('cambiar_password');

    }

    public function show($id)
    {
        $user = User::find($id);
        return view('usuario_profile')->with('user',$user);
    }
    function update(Request $request, $id){
        $user = User::find($id);
        if($request->email != ''){
            $email = $request->email;
        }else{
            $email="";
        }
        if($request->bloqueado != ''){
            $bloqueado = $request->bloqueado;
        }else{
            $bloqueado=false;
        }
        if($request->name != ''){
            $name = $request->name;
        }else{
            $name="0";
        }
        if($request->password != ''){
            $password=Hash::make($request->password);
        }else{
            $password = $user->password;
        }
        if($request->apellido != ''){
            $apellido = $request->apellido;
        }else{
            $apellido ="";
        }
        if($request->codigo != ''){
            $codigo = $request->codigo;
        }else{
            $codigo ="";
        }
        if($request->rol != ''){
            $rol=$request->rol;
        }else{
            $rol=0;
        }

        DB::table('users')->where('id', $id)->update(['name' => $name, 'apellido'=>$apellido,'email'=>$email, 'bloqueado'=>$bloqueado,  'password'=>$password,'codigo'=>$codigo,'rol'=>$rol]);
        // echo $request;

        if($user){
            return redirect('admin/users_list');
        }else{
            return redirect('admin/users_list');
        }
    }
    function update_web(Request $request, $id){

        $user = User::find($id);
        // echo $request;
        // $user->update($request->all());
        if($request->password!=null){
            $password=Hash::make($request->password);
        DB::table('users')->where('id', $id)->update(['password'=>$password]);
        }
        // echo $request;

        if($user){
            return redirect('preparacion');
        }else{
            return redirect('preparacion');
        }
    }

    public function toggleDisable(Request $request)
    {

        $option = $request->disabled;
        $user = $request->user;
        DB::table('users')->where('id', $user)->update(['bloqueado' => $option]);

        $usuarios = User::all();
        return redirect('admin/users_list');
    }

    public function destroy($id)
    {
        // funciona de puta madre
        $user = User::find($id);
        $usuarios = User::all();
        $user->delete();
        return redirect('admin/users_list');
    }
    public function home($id){
        return view('home')->with('usuario', $usuario);
    }
}
