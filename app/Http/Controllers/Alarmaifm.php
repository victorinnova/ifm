<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Alarmaifm extends Controller
{
    public function getTest()
    {
        $db_ext = \DB::connection('comments');
        $alarmas = $db_ext->table('ifm_alarmas')->orderby('id','DESC')->take(10)->get();
        $alarmaid = $db_ext->table('ifm_alarmas')->where('id',1)->first();
        return view('home')->with('alarmas',$alarmas)->with('alarmaid',$alarmaid );
    }
    public function mostrarTabla($id)
    {
        $db_ext = \DB::connection('comments');
        $alarmas = $db_ext->table('ifm_alarmas')->orderby('id','DESC')->take(10)->get();
        $alarmaid = $db_ext->table('ifm_alarmas')->where('id',$id)->first();
        return view('home')->with('alarmas',$alarmas)->with('alarmaid',$alarmaid);
    }
    public function busqueda(request $request)
    {
        $db_ext = \DB::connection('comments');
        $alarmas = $db_ext->table('ifm_alarmas')->where('idalarma',$request->alarma)->whereBetween('startdate',[$request->datepicker,$request->datepicker2])->orderby('id','DESC')->take(1000)->get();
        $alarmaid = $db_ext->table('ifm_alarmas')->where('id',1)->first();
        return view('home')->with('alarmas',$alarmas)->with('alarmaid',$alarmaid)->with('request',$request);
    }
    public function grafica(){
        $db_ext = \DB::connection('comments');
        $alarmas = $db_ext->table('ifm_alarmas')->orderby('id','DESC')->take(10)->get();
        $alarmaid = $db_ext->table('ifm_alarmas')->where('id',1)->first();
        return view('grafica')->with('alarmas',$alarmas);
    }
}   