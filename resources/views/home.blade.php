@extends('layouts.app')

@section('content')
<div class="d-flex" id="wrapper">

<!-- Sidebar -->
 <div class="border-right" style="background-color:#394880; position: fixed;height: 100%;  z-index: 3000;" id="sidebar-wrapper">
  <div class="sidebar-heading" style="padding-left:9%;background-color:white;"><img src="descarga.jpg" alt="" width="170px;" height="32px"></div>
  <!--<div class="list-group list-group-flush border-success">
   <a  id="preparacion" href="{{url('/preparacion')}}" class="list-group-item list-group-item-action text-white border-success menu" style="background-color:#009D60"><i class="fas fa-clipboard-list text-white mr-2"></i>Preparación</a>
    @if(auth()->user()->rol == 1)
    <a  id="usuarios"href="{{url('/admin/users_list')}}"class="list-group-item list-group-item-action text-white  border-success menu" style="background-color:#fd7b00"><i class="fas fa-users text-white mr-2"></i>Usuarios</a>
    <a  id="maquinas" href="{{url('/admin/maquinas_list')}}" class="list-group-item list-group-item-action text-white  border-success menu" style="background-color:#fd7b00"><i class="far fa-hdd text-white mr-2"></i>Maquinas</a>
    <a  id="lotes" href="{{url('/admin/lotes_list')}}" class="list-group-item list-group-item-action text-white  border-success menu" style="background-color:#fd7b00"><i class="fas fa-boxes text-white mr-2"></i>Lotes</a>
    @endif             
  </div>-->
</div>
<!-- /#sidebar-wrapper -->
<style>
.menu:hover{
background-color:#009D60!important;
}
</style>
<!-- Page Content -->
<div id="page-content-wrapper">
    <nav class="navbar navbar-expand-lg navbar-light  border-bottom" style=" background-color:#394880 ; height: 62px; position: fixed;    width: 100%; z-index: 100;">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
      @guest
        <li class="nav-item">
          <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
        </li>
        @else
        <li class="nav-item">
          <a id="navbarDropdown" class="nav-link text-white" aria-haspopup="true" aria-expanded="false" v-pre>
            Usuario: {{ Auth::user()->name }} <span class="caret"></span>
          </a>
        </li>
      @endguest
   
        <li class="nav-item">
          <a class="nav-link text-white" > {{ date('H:i') }} </a>
        </li>
        <!--<li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle"  id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Dropdown
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" >Action</a>
            <a class="dropdown-item" >Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" >Something else here</a>
          </div>
        </li>-->
        <li class="nav-item">
        <?php
          $id=auth()->user()->id;
        ?>
        <a href="{{url('/change_password')}}" class="nav-link"><i class="fas fa-cogs text-white"></i></a>
        </li>
        
        <li class="nav-item">
        <a class="nav-link" href="{{ route('logout') }}"
          onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
          <i class="fas fa-power-off text-white"></i>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
        </form>
        </li>
      </ul>
    </div>
  </nav>
  <form action="/busqueda" method="post">
  @csrf
  <div class="container-fluid" style= "width: 100%; padding-right: 0px; padding-left: 15%; padding-top: 5%;  margin-left: auto;">
    <div class="col-lg-12 row">
      <div class="col-lg-5 row ml-3 mt-4">
          <h5 class="mt-4 ml-5">Alarma</h5>
          <select class="ml-3 w-50 h-50 mt-4" name="alarma">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
          </select>
      </div>
      <div class="col-lg-6 row mt-4">
        <h5 class="mt-4 ml-4">Fecha</h5>
        <input class="ml-3 w-25 h-50 mt-4"  type="date" name="datepicker" id="datepicker">
        <h5 class="mt-4 ml-3">a</h5>
        <input class="ml-3 w-25 h-50 mt-4"  type="date" name="datepicker2" id="datepicker2">
        <button type="submit" class="btn btn-outline-primary mt-3 ml-3"><i class="fas fa-search"></i></button>
      </div>
      </div>
      </form>
      <?php $a = 0?>  
    <div class="col-lg-12 mt-5">
    <table>
						<thead>
							<tr class="table100-head"> 
                <th class="column1">Fecha</th>
								<th class="column2">Id Alarma</th>
								<th class="column3">Descripcion Alarma</th>
								<th class="column4">valor</th>
								<th class="column5">Error</th>
								<th class="column6">Acciones</th>
							</tr>
						</thead>
						<tbody>
            <?php  $a=0?>
            @foreach($alarmas as $alarma)
                  <tr>
                    <td class="column1">{{$alarma->fecha}}</td>
                    <td class="column2">{{$alarma->idalarma}}</td>
                    <td class="column3 ">{{$alarma->descripcion_alarma}}</td>
                    <td class="column4">{{$alarma->valor}}</td>
                    <td class="column5">{{$alarma->error}}</td>
                    <td class="column6"><a type="button" class="btn"  href="/tabla/{{$alarma->id}}" style="background-color:#394880;color:white;">+ Info</a></td>
                  </tr>	
         
      @endforeach					
						</tbody>
           
		</table>
    </div>
    @if($alarmaid->id > 1 )
    <div class="col-lg-12 mt-4">
      <!--Table-->
<table id="tablePreview" class="table">
<!--Table head-->
  <!--Table head-->
  <!--Table body-->  
  <tbody>
  <tr>
      <th scope="row">ID </th>
      <td>{{$alarmaid->id}}</td>
    </tr>
    <tr>
      <th scope="row">ID Instalación</th>
      <td>{{$alarmaid->idInstalacion}}</td>
    </tr>
    <tr>
      <th scope="row">Descripción</th>
      <td>{{$alarmaid->descripcion_alarma}}</td>
    </tr>
    <tr>
      <th scope="row">se01 Peak Valor</th>
      <td>{{$alarmaid->se01_a_peak_valor}}</td>
    </tr>
    <tr>
      <th scope="row">se02 Peak Valor</th>
      <td>{{$alarmaid->se03_a_peak_valor}}</td>
    </tr>
    <tr>
      <th scope="row">se03 Peak Valor</th>
      <td>{{$alarma->se03_a_peak_valor}}</td>
    </tr>
    <tr>
      <th scope="row">se04 Peak Valor</th>
      <td>{{$alarmaid->se04_a_peak_valor}}</td>
    </tr>
    <tr>
      <th scope="row">se02 Des Valor</th>
      <td>{{$alarmaid->se02_des_valor}}</td>
    </tr>
    <tr>
      <th scope="row">se04 Des Valor</th>
      <td>{{$alarmaid->se04_des_valor}}</td>
    </tr>
    <tr>
      <th scope="row">se02 Cavit Valor</th>
      <td>{{$alarmaid->se02_cavit_valor}}</td>
    </tr>
    <tr>
      <th scope="row">se02 Desalineamiento Valor</th>
      <td>{{$alarmaid->se02_desalineamiento_valor}}</td>
    </tr>
    <tr>
      <th scope="row">se04 Desalineamiento Valor</th>
      <td>{{$alarmaid->se04_desalineamiento_valor}}</td>
    </tr>
    <tr>
      <th scope="row">se01 VRMS Valor</th>
      <td>{{$alarmaid->se01_v_rms_valor}}</td>
    </tr>
    <tr>
      <th scope="row">se01 ARMS Valor</th>
      <td>{{$alarmaid->se01_a_rms_valor}}</td>
    </tr>
    <tr>
      <th scope="row">se02 VRMS Valor</th>
      <td>{{$alarmaid->se02_v_rms_valor}}</td>
    </tr>
    <tr>
      <th scope="row">se02 ARMS Valor</th>
      <td>{{$alarmaid->se02_a_rms_valor}}</td>
    </tr>
    <tr>
      <th scope="row">se03 VRMS Valor</th>
      <td>{{$alarmaid->se03_v_rms_valor}}</td>
    </tr>
    <tr>
      <th scope="row">se03 ARMS Valor</th>
      <td>{{$alarmaid->se03_a_rms_valor}}</td>
    </tr>
    <tr>
      <th scope="row">se04 VRMS Valor</th>
      <td>{{$alarmaid->se04_v_rms_valor}}</td>
    </tr>
    <tr>
      <th scope="row">se04 ARMS Valor</th>
      <td>{{$alarmaid->se04_a_rms_valor}}</td>
    </tr>
    <tr>
      <th scope="row">se04 LA Valor</th>
      <td>{{$alarmaid->se04_la_valor}}</td>
    </tr>
    <tr>
      <th scope="row">se03 LOA Valor</th>
      <td>{{$alarmaid->se03_loa_valor}}</td>
    </tr>
    <tr>
      <th scope="row"></th>
      <td class="d-flex flex-row-reverse"><a type="button" href="" class="btn ml-5" style="background-color:#394880;color:white;">Gráfica</a></td>
    </tr>
  </tbody>
  <!--Table body-->
</table>
@endif
<!--Table-->
    </div>
  </div>

@endsection
 <!-- /#wrapper -->
 <style>
	  body {
      overflow-x: hidden;
    }
    p {
        font-family: Helvetica, Arial, sans-serif;
        font-weight: lighter;
    }
    input[type=text]{
      border-bottom: 1px solid #B1B1B1;
    }

    button {
        font-family: Helvetica, Arial, sans-serif;
        font-size: 1.0em;    
    }

    div#progressBar {
        width: 600px; 
        height: 90px;
    }


    #sidebar-wrapper {
      min-height: 100vh;
      margin-left: -15rem;
      -webkit-transition: margin .25s ease-out;
      -moz-transition: margin .25s ease-out;
      -o-transition: margin .25s ease-out;
      transition: margin .25s ease-out;
    }

    #sidebar-wrapper .sidebar-heading {
      padding: 0.875rem 1.25rem;
      font-size: 1.2rem;
    }

    #sidebar-wrapper .list-group {
      width: 15rem;
    }

    #page-content-wrapper {
      min-width: 100vw;
    }

    #wrapper.toggled #sidebar-wrapper {
      margin-left: 0;
    }

    @media (min-width: 768px) {
      #sidebar-wrapper {
        margin-left: 0;
      }

      #page-content-wrapper {
        min-width: 0;
        width: 100%;
      }

      #wrapper.toggled #sidebar-wrapper {
        margin-left: -15rem;
      }
    }
    .limiter {
  width: 100%;
  margin: 0 auto;
}

.container-table100 {
  width: 100%;
  min-height: 100vh;


  display: -webkit-box;
  display: -webkit-flex;
  display: -moz-box;
  display: -ms-flexbox;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  
}

.wrap-table100 {
  width: 1170px;
}

table {
  border-spacing: 1;
  border-collapse: collapse;
  background: white;
  border-radius: 10px;
  overflow: hidden;
  width: 100%;
  margin: 0 auto;
  position: relative;
}
table * {
  position: relative;
}
table td, table th {
  padding-left: 8px;
}
table thead tr {
  height: 60px;
  background: #394880;
}
table tbody tr {
  height: 50px;
  background:#EEEEEE;
}
table tbody tr:last-child {
  border: 0;
}
table td, table th {
  text-align: left;
}
table td.l, table th.l {
  text-align: right;
}
table td.c, table th.c {
  text-align: center;
}
table td.r, table th.r {
  text-align: center;
}


.table100-head th{
  font-size: 18px;
  color: #fff;
  line-height: 1.2;
  font-weight: unset;
}

tbody tr:nth-child(even) {
  background-color: #f5f5f5;
}

tbody tr {
  font-size: 15px;
  color: #808080;
  line-height: 1.2;
  font-weight: unset;
}

tbody tr:hover {
  color: #555555;
  background-color: #f5f5f5;
  cursor: pointer;
}

.column1 {
  text-align:center;
}

.column2 {
  text-align:center;
}

.column3 {
 text-align:center;
}

.column4 {
  text-align: center;
}

.column5 {
  text-align: center;
}
.column6{
  text-align:center;
}

@media screen and (max-width: 992px) {
  table {
    display: block;
  }
  table > *, table tr, table td, table th {
    display: block;
  }
  table thead {
    display: none;
  }
  table tbody tr {
    height: auto;
    padding: 37px 0;
  }
  table tbody tr td {
    margin-bottom: 24px;
  }
  table tbody tr td:last-child {
    margin-bottom: 0;
  }
  table tbody tr td:before {
    font-family: OpenSans-Regular;
    font-size: 14px;
    color: #999999;
    line-height: 1.2;
    font-weight: unset;
    position: absolute;
    width: 40%;
    left: 30px;
    top: 0;
  }
  table tbody tr td:nth-child(1):before {
    content: "Fecha";
  }
  table tbody tr td:nth-child(2):before {
    content: "ID Alarma";
  }
  table tbody tr td:nth-child(3):before {
    content: "Descripcion Alarma";
  }
  table tbody tr td:nth-child(4):before {
    content: "Valor";
  }
  table tbody tr td:nth-child(5):before {
    content: "Error";
  }
  table tbody tr td:nth-child(6):before {
    content: "Acciones";
  }

  .column4,
  .column5,
  .column6 {
    text-align: center;
  }

  .column4,
  .column5,
  .column6,
  .column1,
  .column2,
  .column3 {
    width: 100%;
   
  }

  tbody tr {
    font-size: 14px;
  }
}

@media (max-width: 576px) {
  .container-table100 {
    padding-left: 15px;
    padding-right: 15px;
  }
}
  </style>

    <!-- Menu Toggle Script -->
    <script>
      $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
      });
      $(function(){
    $("#datepicker").datepicker({
        changeMonth: true,
        changeYear: true
    });
});
  </script>