@extends('layouts.app')

@section('content')
<style>
            html, body {
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }


            .content {
                height: -webkit-fill-available!important;
                text-align: center;
                background-color:#EEEEEE;
                width:100%;
                height:100%;
                display:flex;
                align-content:center;
                align-items:center;
                justify-content:center;
                flex-direction:column;
            }


            .formulario {
            border: none;
            padding: 16px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            transition-duration: 0.4s;
            cursor: pointer;
            margin-top:20px;
}
            }
            
            .m-b-md {
                margin-bottom: 30px;
            }
           
        </style>
        <div class="content">
        <div class="flex-center position-ref ">
        <div class="container">
    <div class="row justify-content-center">
        
            <div class="card" style="width:500px;height:440px;margin-top:4%;">
                <div class="card-header text-white" style="display:flex;justify-content:center;align-items:center;background-color:#394880;height:67px;padding-top:4%;">{{ __('Introduzca sus datos :') }}</div>

                <div class="card-body" style="margin-top:3%;">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        
                        <div class="form-group row">

                            <label for="usercode" class="col-md-6 col-form-label " style="color:#394880;margin-left:8px;">{{ __('E-Mail') }}</label>


                            <div class="col-md-8">
                                <input id="email" style="margin-left:30%;" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-5 col-form-label text-md-right" style="color:#394880 ;">{{ __('Contraseña') }}</label>

                            <div class="col-md-8">
                                <input id="password" style="margin-left:30%;" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row" style="padding-right:52%;">
                            <div class="col-md-8 offset-md-4" >
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" style="color:#394880" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mt-1" style="padding-right:24%;">
                            <div class="col-md-8 offset-md-4" >
                                <button type="submit" style="background-color:#394880; border: 1px solid #394880;width:200px;" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>
                   
                    </form>
                </div>
            </div>
       
    </div>
</div>
        </div>
        </div>
    
@endsection
