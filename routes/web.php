<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@login')->name('login');
Route::get('/admin', 'HomeController@admin')->name('admin');
Auth::routes();

Route::group(['middleware'=>'auth:web'],function(){
    Route::group(['middleware'=>'admin'],function(){
        Route::get('/admin', 'HomeController@admin')->name('admin');
        Route::get('/admin/users_list', 'UserCustomController@showAll');
        Route::get('/admin/maquinas_list', 'MaquinaController@index');
        Route::get('/admin/lotes_list', 'LoteController@index');
        Route::get('/admin/edit/{id}','UserCustomController@index');
        Route::get('/admin/create','UserCustomController@create');
        //Route::get('/admin/maquina/create','MaquinaController@create');                
        Route::post('/admin/usuarios/create', 'UserCustomController@create_web');
        Route::post('/admin/usuarios/update/{id}', 'UserCustomController@update');
    });
    Route::get('/change_password', 'UserCustomController@show_web');
    Route::post('/cambiar_password/update/{id}', 'UserCustomController@update_web');
    Route::get('/home','Alarmaifm@getTest');
    Route::get('/tabla/{id}','Alarmaifm@mostrarTabla');
    Route::post('/busqueda','Alarmaifm@busqueda');
    Route::get('/grafica','Alarmaifm@grafica');
    Route::get('/mail', 'AlarmaController@Mail');
  
});

Route::get('admin/routes' ,'HomeController@admin')->middleware('admin');
